import WelcomePage from '../Components/WelcomePage';

export default function Error() {

    const data = {
        
        title: " 404 - Page Not Found!",
        content: "The page you are looking for cannot not found",
        destination: "/",
        label: "Back home"       
    }

    return (
        <WelcomePage data={data}/>
    )    
} 