
import Highlights from '../Components/Highlights';
import Carousel from 'react-bootstrap/Carousel';
import { Button, Container } from 'react-bootstrap';

export default function Home(){
    return(
        <>
        <Carousel className='firstContent' fade>

            <Carousel.Item>
                <img
                className="w-100"
                src="images/4.png"
                alt="First slide"
                /> 
                <Carousel.Caption>
                    <Button className="btn1" variant='light' style={{ marginTop: '-25%' }}>Shop Now</Button>
                    </Carousel.Caption>
            </Carousel.Item>
                
            <Carousel.Item>
                <img
                className="w-100"
                src="images/2.png"
                alt="second slide"
                /> 
                <Carousel.Caption>
                    <Button className="btn1" variant='light' style={{ marginTop: '-25%' }}>Shop Now</Button>
                    </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
                <img
                className="w-100"
                src="images/3.png"
                alt="third slide"
                /> 
                <Carousel.Caption>
                    <Button className="btn1" variant='light' style={{ marginTop: '-25%' }}>Shop Now</Button>
                    </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
                <img
                className="w-100"
                src="images/1.png"
                alt="Fourt slide"
                /> 
                <Carousel.Caption>
                    <Button className="btn1" variant='light' style={{ marginTop: '-25%' }}>Shop Now</Button>
                    </Carousel.Caption>
            </Carousel.Item>

            <Carousel.Item>
                <img
                className="w-100"
                src="images/5.png"
                alt="Fifth slide"
                /> 
                <Carousel.Caption>
                    <Button className="btn1" variant='light' style={{ marginTop: '-25%' }}>Shop Now</Button>
                    </Carousel.Caption>
            </Carousel.Item>
        </Carousel>

        
        <Container>
            <Highlights/> 
        </Container>
            <footer>
			    <div className="foot text-center p-2">Copyright 2023 - Nate Clothing</div>
		    </footer>      
      </>
    )
}