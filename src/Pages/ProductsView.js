import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { useParams, Link, useNavigate  } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView() {
	const { productId } = useParams();
	const navigate = useNavigate()

	const { user } = useContext(UserContext);

	const [ name, setName ] = useState('');
	const [ description, setDescription ] = useState('');
	const [ price, setPrice ] = useState(0);
	const [ quantity, setQuantity] = useState(0);

	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log('products');
			console.log(data);

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
		})
	}, [productId]);

	

	const order = (productId, quantity, productName) => {

		fetch(`${process.env.REACT_APP_API_URL}/cart/addToCart`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data) {
				Swal.fire({
					title: "Added to cart!",
					icon: "success",
					text: `${productName} successfully added to your cart!`,
					willClose: () => {
						navigate(`/products`)
					}

				})
			
			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	const handleDecrement = () => {
		setQuantity(quantity - 1)
	}

	const handleIncrement = () => {
		if (user.id !== null && user.isAdmin === false) {
			setQuantity(quantity + 1)
		}
	}

	return (

		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3}}>
					<Card>
						<Card.Body className="text-center">
					        <Card.Title>{name}</Card.Title>
					        <Card.Subtitle>Description</Card.Subtitle>
					        <Card.Text>{description}</Card.Text>
					        <Card.Subtitle>Price</Card.Subtitle>
					        <Card.Text>Php {price}</Card.Text>
					        <Card.Subtitle>Quantity:</Card.Subtitle>
					        <div className="input-group mt-2 mb-3">
					        	<Button type="button" onClick={() => handleDecrement(productId)} className="input-group-text">-</Button>
					        	<div className="form-control text-center">{quantity}</div>
					        	<Button type="button" onClick={() => handleIncrement(productId)} className="input-group-text">+</Button>
					        </div>
					        
					        {user.id !== null
					        	?
					        	<Button variant="primary" onClick={() => order(productId, quantity, name)}>Add to cart</Button>
					        	:
					        	<Button as={Link} to="/login" variant="danger">Log in to Add to cart</Button>
					    	}
						</Card.Body>
					</Card>
				</Col>
			</Row>
		</Container>
	)
}