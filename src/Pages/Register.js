import { useState, useEffect, useContext } from 'react';
import { Form, Button, Card } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register(){

	const {user} = useContext(UserContext);
	
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password1);
	console.log(password2);

	function registerUser(e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				"Content-Type" : "application/json"
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {

				Swal.fire({
					title: "Duplicate email found!",
					icon: "error",
					text: "Kindly provide another email to complete the registration."
				})

			} else {
				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						"Content-Type": "application/json"
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data);

					if(data === true){
						Swal.fire({
							title: "Registration Successful!",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						setFirstName('');
						setLastName('');
						setEmail('');
						setPassword1('');
						setPassword2('');

					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again."
						})
					}
				})
			}
		})	

	}

	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== ''  && password1 !== '' & password2 !== '') && (password1 === password2)) {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, password1, password2]);

	return	(
		(user.id !== null)
		?
			<Navigate to="/courses"/>
		:
		<div className=" container-fluid row bglog">
			<div className="row col-sm-12 col-md-4 col-lg-3">
            
            </div>
			<div className="row col-sm-12 col-md-4 col-lg-6">
			<Card className="login" style={{ height: '55rem', width: '100%'}}>
			<Card.Img variant="top" src="images/loginbg1.png" style={{ height: '14rem'}}/>
			<h1 className="text-center titlelog">REGISTER</h1>
            <p className="text-center subT">Register using social networks</p>
            <div className="Col text-center">
            <img className='' src="/images/google.png" alt="" style={{ height: '30px', weight: '15px'}}/>
            <img className='icon' src="/images/facebook.png" alt="" style={{ height: '30px', weight: '15px'}}/>
            <img className='' src="/images/linkedin.png" alt="" style={{ height: '30px', weight: '15px'}}/>
            <p>──────────────────────  or  ───────────────────────</p>
            </div>
			<Form onSubmit={(e) => registerUser(e)}>
			<Form.Group controlId="firstName">
			    <Form.Label>First Name</Form.Label>
			    <Form.Control 
			        type="text" 
			        placeholder="Enter first name"			        
			        required
			        value={firstName} 
			        onChange={e => setFirstName(e.target.value)}
			    />
			</Form.Group>

			<Form.Group controlId="lastName">
			    <Form.Label>Last Name</Form.Label>
			    <Form.Control 
			        type="text" 
			        placeholder="Enter last name"			        
			        required
			        value={lastName} 
			        onChange={e => setLastName(e.target.value)}
			    />
			</Form.Group>

			<Form.Group controlId="userEmail">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					required
					value={email}
					onChange={e => setEmail(e.target.value)}
				/>
				<Form.Text className="text-muted">We'll never share your email with anyone else.</Form.Text>			
			</Form.Group>

			<Form.Group controlId="password1">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					required
					value={password1}
					onChange={e => setPassword1(e.target.value)}
				/>			
			</Form.Group>

			<Form.Group controlId="password2">
				<Form.Label>Verify Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Verify password"
					required
					value={password2}
					onChange={e => setPassword2(e.target.value)}
				/>		
			</Form.Group> <br></br>

			{/*conditional render submit button based on isActive state*/}

			{isActive	
				?
					<Button style={{ width: '100%'}} variant="primary" type="submit" id="submitBtn">Submit</Button>
				:
					<Button style={{ width: '100%'}} variant="danger" type="submit" id="submitBtn" disabled>Submit</Button>
			}
			
		</Form>
			</Card>
			</div>
		</div>
		

			// section ---------------------------------------section
		
	)
}