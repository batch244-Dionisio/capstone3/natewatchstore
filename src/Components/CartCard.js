import { Link } from 'react-router-dom'
import { Card, Button, Container, Row } from 'react-bootstrap'
import CardGroup from 'react-bootstrap/CardGroup';

export default function CartCard({cartProp}) {
    const { productId, productName, quantity, isActive, totalAmount, purchasedOn } = cartProp
 

    return (
        <CardGroup>
        <Card style={{ width: '100rem' }}>
            <Card.Img className="w-100" src="images/.png" alt=""/>   
            <Card.Body>
            <Card.Title>{productName}</Card.Title>
            <Card.Subtitle>Description</Card.Subtitle>
                <Card.Text>{quantity}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>PhP {totalAmount}</Card.Text>
                {/* <Button as={Link} variant="warning" to={`/products/${_id}`}>Add to Cart</Button> */}
            </Card.Body>
        </Card>
            </CardGroup>
    )
}