import { Col, Row, Card, Button } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function WelcomePage() {
    return (
        <Col className='text-center'>
        <p className='welcome'> Welcome to Nate Clothing </p>
        <p className='para1'>Fashion is part of the daily air and it changes all the time, with all the events. You can even see the approaching of a revolution in clothes. You can see and feel everything in clothes</p>
        <Row>
            <div className='w-50'>
                <Card.Img variant="top" src="images/section2b.jpg" />
                <Card.Body>
                    <h1 className='pb-3 pt-4 TiCloth'>Men's Clothing</h1>
                    <Card.Text>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    </Card.Text>
                    <Button as={Link} to="/products" variant="secondary">Click Here</Button>
                </Card.Body>
            </div> 
            <div className='w-50'>
                <Card.Img variant="top" src="images/section2a.jpg"/>
                <Card.Body>
                <h1 className='pb-3 pt-4 TiCloth'>Women's Clothing</h1>
                    <Card.Text>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                    </Card.Text>
                    <Button as={Link} to="/products" variant="secondary">Click Here</Button>
                </Card.Body>
            </div> 
        </Row>

        <h1 className='Shopcat'>SHOP BY CATEGORY</h1>
            <Col>
            <img className='s3pic' src="/images/stripes1.png" alt=""/>
            <img className='s3pic2' src="/images/peach1.png" alt=""/>
            <img className='s3pic' src="/images/pink1.png" alt=""/>
            </Col>

            <h1 className='arriv'>NEW ARRIVALS</h1>
            <div className="d-flex justify-content-around text-start">
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/target1.png" />
                <Card.Body>
                <Card.Title>Target Male Shirt</Card.Title>
                <Card.Text>
                    Frappe Embroidery Regular Fit T-Shirt <br/><br/>
                    PHP 599.00
                </Card.Text>
                </Card.Body>
                <Button as={Link} to="/products" variant="secondary">Add to Cart</Button>
            </Card>
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/brown1.png" />
                <Card.Body>
                <Card.Title>OXGN female Shirt</Card.Title>
                <Card.Text>
                Textured Slim Regular Fit T-Shirt <br/><br/>
                Php 499.00
                </Card.Text>
                </Card.Body>
                <Button as={Link} to="/products" variant="secondary">Add to Cart</Button>
            </Card>
            <Card className='mb-5' style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/red1.png" />
                <Card.Body>
                <Card.Title>OXGN female Shirt</Card.Title>
                <Card.Text>
                Textured Slim Regular Fit T-Shirt <br/><br/>
                Php 499.00
                </Card.Text>
                </Card.Body>
                <Button as={Link} to="/products" variant="secondary">Add to Cart</Button>
            </Card>   
            </div>
            <div className="d-flex justify-content-around text-start">
            <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/checkered1.png" />
                <Card.Body>
                <Card.Title>DeFacto</Card.Title>
                <Card.Text>
                Regular Fit Plaid Long Sleeve Shirt <br/><br/>
                Php 2,104.00
                </Card.Text>
                </Card.Body>
                <Button as={Link} to="/products" variant="secondary">Add to Cart</Button>
            </Card>
            <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/polo1.png" />
                <Card.Body>
                <Card.Title>H&M</Card.Title>
                <Card.Text>
                Relaxed Fit Short-Sleeved Shirt with Iconic Design <br/><br/>
                Php 999.00
                </Card.Text>
                </Card.Body>
                <Button as={Link} to="/products" variant="secondary">Add to Cart</Button>
            </Card>
            <Card style={{ width: '18rem' }}>
                <Card.Img variant="top" src="images/stripes1.png" />
                <Card.Body>
                <Card.Title>Cotton On</Card.Title>
                <Card.Text>
                Cuban stripes Short Sleeve Shirt <br/><br/>
                Php 1,599.00
                </Card.Text>
                </Card.Body>
                <Button as={Link} to="/products" variant="secondary">Add to Cart</Button>
            </Card>   
            </div>
              <Button as={Link} to="/products" variant='dark' className='mt-5 mb-5 px-4'>SHOP MORE</Button>
              <p className='fashion mt-5'>FIND YOUR FASHION AT</p>
              <p className='fasH'>NATE CLOTHING</p>
              <p className='fashp'>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
              
            <div className='row justify-content-center'>
                <div style={{ width: '18rem' }}>
                    <Card.Body>
                    <Card.Img src="images/logo.png"  style={{ width: '10rem',  paddingTop: '40px', paddingRight: '25px' }}/>
                    </Card.Body>
                </div>
                <div style={{ width: '18rem', marginLeft: '90px', marginRight: '90px' }}>
                    <Card.Body>
                    <h4 className='pb-3 text-start'>Features</h4>
                    <Card.Text className='text-start pb-5'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua tempor incididunt ut labore et dolore magna  
                    </Card.Text> 
                    </Card.Body>
                </div>
                <div style={{ width: '18rem', paddingTop: '60px'}}>
                    <Card.Body>
                    <img className='' src="/images/fb.png" alt="" style={{ height: '50px', weight: '15px'}}/>
                     <img className='icon' src="/images/skype.png" alt="" style={{ height: '40px', weight: '15px'}}/>
                    <img className='' src="/images/ig.png" alt="" style={{ height: '50px', weight: '15px'}}/>
                    <img className='' src="/images/twitter.png" alt="" style={{ height: '50px', weight: '15px'}}/>
                    </Card.Body>
                </div>
            </div>
        </Col>
    )
}